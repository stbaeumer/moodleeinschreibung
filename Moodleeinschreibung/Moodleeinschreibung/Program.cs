﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Moodleeinschreibung
{
    class Program
    {
        public const string ConnectionStringUntis = @"Provider = Microsoft.Jet.OLEDB.4.0; Data Source=M:\\Data\\gpUntis.mdb;";

        static void Main(string[] args)
        {
            Global.Schulnummer = 177659;
            int sj = (DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1);
            string aktSjUntis = sj.ToString() + (sj + 1);
        
            Periodes periodes = new Periodes(ConnectionStringUntis, aktSjUntis);
            Lehrers lehrers = new Lehrers(aktSjUntis, ConnectionStringUntis, periodes);

            Console.WriteLine("Geben Sie den Kurskurznamen kommasepariert ein, in den/en alle Lehrer eingeschrieben werden sollen. GROSS- und KLEINschreibung von Kurskurznamen beachten! UPLOAD-TYP: Neue hinzufügen und vorhandene aktualisieren: ");            
            string kursKurzname = Console.ReadLine();

            List<object> schuelerListe = new List<object>();

            var x = (from l in lehrers                                          
                     where l.Kürzel.Length > 1
                     select new
                     {
                         username = l.Kürzel,
                         password = "123",
                         lastname = l.Nachname,
                         firstname = l.Vorname,
                         email = l.Mail,
                         auth = "ldap",
                         course1 = kursKurzname,
                         group1 = "BKB-Lehrer",
                         department = "BKB-Lehrer",
                         city = DateTime.Now.ToString("yyyyMMdd")
                     }).ToList();

            schuelerListe.AddRange(x);

            string dateinameUndPfad = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" +  DateTime.Now.ToString("yyyyMMddHHmmss") + "-moodle-einschreibung-" + kursKurzname + ".csv";

            Console.WriteLine("[?] Moodle im Browser öffnen? (j/n)");

            if ((Console.ReadKey(true)).Key == ConsoleKey.J)
            {
                System.Diagnostics.Process.Start("https://bk-borken.lms.schulon.org/admin/tool/uploaduser/index.php");
            }

            CsvAusgabe(dateinameUndPfad, schuelerListe, Encoding.GetEncoding("UTF-8"), '"');
            Console.ReadKey();
        }

        public static void CsvAusgabe(string pfadUndDateiname, IEnumerable<Object> objektListe, Encoding encoding, char trennzeichen)
        {
            if (objektListe.Count() > 0)
            {
                int index = 0;
                int anzahl = 0;

                while (File.Exists(pfadUndDateiname))
                {
                    Console.Write("Die Datei " + pfadUndDateiname + " existiert bereits. Bitte löschen.'x' für automatisches Löschen. Dann ENTER.");
                    if ((Console.ReadKey(true)).Key == ConsoleKey.X)
                    {
                        File.Delete(pfadUndDateiname);
                    }
                    else
                    {
                        Process.Start(Path.GetDirectoryName(pfadUndDateiname));
                    }
                }

                using (FileStream fs = new FileStream(pfadUndDateiname, FileMode.CreateNew))
                {
                    using (StreamWriter writer = new StreamWriter(fs, encoding))
                    {
                        Console.WriteLine("Datei wird erstellt: " + pfadUndDateiname);
                        foreach (var item in objektListe)
                        {
                            Type myType = item.GetType();

                            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

                            string zeile = "";

                            if (index == 0)
                            {
                                string kopfzeile = "";

                                foreach (PropertyInfo prop in props)
                                {
                                    kopfzeile = kopfzeile + trennzeichen + prop.Name + trennzeichen + ";";
                                }

                                writer.WriteLine(kopfzeile.TrimEnd(';'));
                            }

                            index++;

                            foreach (PropertyInfo prop in props)
                            {
                                zeile = zeile + trennzeichen + prop.GetValue(item, null) + trennzeichen + ";";
                            }
                            anzahl++;
                            writer.WriteLine(zeile.TrimEnd(';'));
                        }
                    }
                }

                EditorOeffnen(pfadUndDateiname);
            }
        }

        public static void EditorOeffnen(string pfad)
        {
            try
            {
                System.Diagnostics.Process.Start(@"C:\Program Files (x86)\Notepad++\Notepad++.exe", pfad);
            }
            catch (Exception)
            {
                System.Diagnostics.Process.Start("Notepad.exe", pfad);
            }
        }
    }
}
