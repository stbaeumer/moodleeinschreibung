﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Text;

namespace Moodleeinschreibung
{
    internal class Global
    {        
        public static int Schulnummer { get; internal set; }

     
        public static string SafeGetString(OleDbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }

        internal static void CsvAusgabe(object ini, string dateinameUndPfad, List<object> schuelerListe, Encoding encoding, char v)
        {
            throw new NotImplementedException();
        }
    }
}